//
//  WTAppDelegate+WTViewControllerRoutines.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTAppDelegate.h"

@interface WTAppDelegate (WTViewControllerRoutines)

+ (void)instantiateRootViewControllerByAuthorizationStatus;
+ (void)proceedToAppWithUserName:(NSString *)username;
+ (void)proceedToAuthorization;

@end
