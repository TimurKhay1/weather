//
//  NSUserDefaults+WTData.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "NSUserDefaults+WTData.h"

static NSString * const kNSUserDefaultsUsernameKey = @"username";

@implementation NSUserDefaults (WTData)

- (void)setWT_username:(NSString *)WT_username {
    if (WT_username)
        [[NSUserDefaults standardUserDefaults] setValue:WT_username forKey:kNSUserDefaultsUsernameKey];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUserDefaultsUsernameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)WT_username {
    return [[NSUserDefaults standardUserDefaults] valueForKey:kNSUserDefaultsUsernameKey];
}

@end
