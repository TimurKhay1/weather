//
//  WTWeatherRequest+Additions.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTWeatherRequest.h"

@interface WTWeatherRequest (Additions)

@property (strong,nonatomic) NSDictionary *weatherDictionary;

@end
