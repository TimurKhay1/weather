//
//  WTUser+CoreDataProperties.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WTUser+CoreDataProperties.h"

@implementation WTUser (CoreDataProperties)

@dynamic firstName;
@dynamic lastName;
@dynamic password;
@dynamic username;
@dynamic weatherRequests;

@end
