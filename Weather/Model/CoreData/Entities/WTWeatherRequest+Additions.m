//
//  WTWeatherRequest+Additions.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTWeatherRequest+Additions.h"

@implementation WTWeatherRequest (Additions)

- (NSDictionary *)weatherDictionary {
    return (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:self.weather];
}

- (void)setWeatherDictionary:(NSDictionary *)weatherDictionary {
    self.weather = [NSKeyedArchiver archivedDataWithRootObject:weatherDictionary];
}

@end
