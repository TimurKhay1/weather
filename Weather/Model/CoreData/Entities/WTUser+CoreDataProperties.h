//
//  WTUser+CoreDataProperties.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WTUser.h"
#import "WTWeatherRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface WTUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *password;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSSet<WTWeatherRequest *> *weatherRequests;

@end

@interface WTUser (CoreDataGeneratedAccessors)

- (void)addWeatherRequestsObject:(WTWeatherRequest *)value;
- (void)removeWeatherRequestsObject:(WTWeatherRequest *)value;
- (void)addWeatherRequests:(NSSet<WTWeatherRequest *> *)values;
- (void)removeWeatherRequests:(NSSet<WTWeatherRequest *> *)values;

@end

NS_ASSUME_NONNULL_END
