//
//  WTUser+Additions.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTUser+Additions.h"
#import "NSUserDefaults+WTData.h"
#import "MagicalRecord/MagicalRecord.h"

@implementation WTUser (Additions)

+ (WTUser *)currentUser {
    NSString *username = [NSUserDefaults standardUserDefaults].WT_username;
    return username ? [WTUser MR_findFirstByAttribute:@"username" withValue:username] : nil;
}

+ (WTUser *)currentUserInContext:(NSManagedObjectContext *)context {
    NSString *username = [NSUserDefaults standardUserDefaults].WT_username;
    return username ? [WTUser MR_findFirstByAttribute:@"username" withValue:username inContext:context] : nil;
}

@end
