//
//  WTUser+Additions.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTUser.h"

@interface WTUser (Additions)

+ (WTUser *)currentUser;
+ (WTUser *)currentUserInContext:(NSManagedObjectContext *)context;

@end
