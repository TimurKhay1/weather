//
//  WTWeatherRequest+CoreDataProperties.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "WTWeatherRequest.h"
#import "WTUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface WTWeatherRequest (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSData *weather;
@property (nullable, nonatomic, retain) WTUser *user;

@end

NS_ASSUME_NONNULL_END
