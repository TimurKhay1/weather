//
//  NSUserDefaults+WTData.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (WTData)

@property (strong,nonatomic) NSString *WT_username;

@end
