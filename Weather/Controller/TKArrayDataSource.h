//
//  TKArrayDataSource.m
//  HotelLook
//
//  Created by Timur on 25.11.15.
//  Copyright © 2015 Timur Khayrullin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^TKArrayDataSourceConfigureCellBlock)(UITableViewCell *cell, id item);

@protocol TKArrayDataSourceSorting <NSObject>

@property (strong,nonatomic) NSSortDescriptor *sortDescriptor;

@end

@interface TKArrayDataSource : NSObject <UITableViewDataSource, TKArrayDataSourceSorting>

- (instancetype)initWithTableView:(UITableView *)tableView
                            items:(NSArray *)items
               cellIdentifier:(NSString *)cellIdentifier
           configureCellBlock:(TKArrayDataSourceConfigureCellBlock)configureCellBlock;

@property (weak,nonatomic) UITableView *tableView;
@property (strong,nonatomic) NSArray *items;
@property (strong,nonatomic) NSString *cellIdentifier;
@property (copy,nonatomic) TKArrayDataSourceConfigureCellBlock configureCellBlock;

@end
