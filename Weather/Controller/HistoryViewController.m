//
//  HistoryViewController.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "HistoryViewController.h"
#import "WTTableViewController.h"
#import "MagicalRecord/MagicalRecord.h"
#import "WTUser+Additions.h"
#import "WTWeatherRequest+Additions.h"
#import "WTAppDelegate+WTViewControllerRoutines.h"

static NSString * const kHistoryViewControllerWeatherRequestSegueIdentifier = @"WeatherRequestHistory";

@interface HistoryViewController ()

@property (strong,nonatomic) NSDateFormatter *dateFormatter;

@end

@implementation HistoryViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // TKCoreDataViewController
    self.fetchedResultsController = [WTWeatherRequest MR_fetchAllGroupedBy:nil
                                                             withPredicate:[NSPredicate predicateWithFormat:@"user == %@", [WTUser currentUser]]
                                                                  sortedBy:@"date"
                                                                  ascending:NO
                                                                  delegate:self];
    self.cellIdentifier = @"WeatherRequest";
    
    // DateFormatter
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.doesRelativeDateFormatting = YES;
    _dateFormatter.locale = [NSLocale currentLocale];
    _dateFormatter.dateStyle = NSDateFormatterShortStyle;
    _dateFormatter.timeStyle = NSDateFormatterShortStyle;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:YES];
}

#pragma mark - User Interactions

- (IBAction)didPressSignOutButton:(UIBarButtonItem *)sender {
    [WTAppDelegate proceedToAuthorization];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kHistoryViewControllerWeatherRequestSegueIdentifier]) {
        if ([segue.destinationViewController isKindOfClass:[WTTableViewController class]]) {
            WTTableViewController *tableViewController = (WTTableViewController *)segue.destinationViewController;
            NSIndexPath* indexPath = [self.tableView indexPathForCell:(UITableViewCell*)sender];
            tableViewController.weather = ((WTWeatherRequest *)[self.fetchedResultsController objectAtIndexPath:indexPath]).weatherDictionary;
            tableViewController.hisory = YES;
        }
    }
}


#pragma mark - TKCoreDataViewController overriding

- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    WTWeatherRequest * weatherRequest = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [self.dateFormatter stringFromDate:weatherRequest.date];
}

@end
