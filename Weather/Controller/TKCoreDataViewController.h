//
//  TKCoreDataViewController.h
//  Banana
//
//  Created by Timur on 01.09.15.
//  Copyright (c) 2015 Timur Khayrullin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

/**
 *  The `TKCoreDataViewController` is used as base class
 *  for TableViewController with FetchResultController
 *
 * Compatable with iOS7,8,9 with Apple's bug fix when targeting to iOS8
 *
 *  @warning This class is intended to be subclassed. You should not use it directly.
 */
@interface TKCoreDataViewController : UITableViewController <NSFetchedResultsControllerDelegate>

/**
 * FetchedResultsController for TableViewController.
 *
 *  @warning Must be set in subclass. Typically in viewDidLoad
 */
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

/**
 * CellIdentifier of cell in TableViewController.
 * Can be only one.
 *
 *  @warning Must be set in subclass. Typically in viewDidLoad
 */
@property (strong, nonatomic) NSString *cellIdentifier;

/**
 * Manual refreshing data of FetchedResultsController.
 * Typically you dont need to use it
 *
 */
- (void)performFetch;

/**
 * Method for configuring cell with your data.
 *
 *  @warning Must be overrided in subclass
 */

- (void)configureCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath;

@end
