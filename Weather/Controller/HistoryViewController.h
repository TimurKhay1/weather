//
//  HistoryViewController.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKCoreDataViewController.h"

@interface HistoryViewController : TKCoreDataViewController

@end
