//
//  TKArrayDataSource.m
//  HotelLook
//
//  Created by Timur on 25.11.15.
//  Copyright © 2015 Timur Khayrullin. All rights reserved.
//

#import "TKArrayDataSource.h"

@implementation TKArrayDataSource

- (instancetype)init {
    NSAssert(NO, @"%s is not a valid initializer for %@.", __PRETTY_FUNCTION__, [self class]);
    return nil;
}

- (instancetype)initWithTableView:(UITableView *)tableView
                            items:(NSArray *)items
                   cellIdentifier:(NSString *)cellIdentifier
               configureCellBlock:(TKArrayDataSourceConfigureCellBlock)configureCellBlock {
    self = [super init];
    
    if (self) {
        _tableView = tableView;
        _items = items;
        _cellIdentifier = cellIdentifier;
        _configureCellBlock = configureCellBlock;
    }
    
    return self;
}


#pragma mark - Properties

- (void)setItems:(NSArray *)items {
    if (self.sortDescriptor)
        items = [items sortedArrayUsingDescriptors:@[self.sortDescriptor]];
    _items = items;
    [self.tableView reloadData];
}


#pragma mark - <UITableViewDataSource>

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.items[indexPath.row];
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    id cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier
                                              forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.configureCellBlock(cell,item);
    return cell;
}


#pragma mark - <TKArrayDataSourceSorting>

@synthesize sortDescriptor = _sortDescriptor;

- (void)setSortDescriptor:(NSSortDescriptor *)sortDescriptor {
    _sortDescriptor = sortDescriptor;
    self.items = self.items;
}

@end
