//
//  LogInViewController.m
//  Weather
//
//  Created by Timur on 28.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "LogInViewController.h"
#import "WTAppDelegate+WTViewControllerRoutines.h"
#import "MagicalRecord/MagicalRecord.h"
#import "WTUser.h"

@implementation LogInViewController

#pragma mark - Overriding

+ (NSArray *)textFieldDictionaries {
    return @[
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"Username",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"Username",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeNone),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @NO
               },
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"Password",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"Password",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeNone),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @YES
               }
             ];
}

- (void)didQuestionaryReady {
    NSString *username = [[self textAtIndex:0] lowercaseString];
    NSString *password = [self textAtIndex:1];
    
    NSPredicate *userPredicate = [NSPredicate predicateWithFormat:@"username == %@ && password == %@", username, password];
    if (![[WTUser MR_findAllWithPredicate:userPredicate] count]) {
        [self showWrongPasswordOrUserNameAlert];
        return;
    }
    
    [WTAppDelegate proceedToAppWithUserName:username];
}

#pragma mark - User Interaction

- (IBAction)didPressLogInButton:(UIBarButtonItem *)sender {
    [self didQuestionaryReady];
}


#pragma mark - Helpers methods

- (void)showWrongPasswordOrUserNameAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Wrong Password or Username"
                                                                             message:@"Enter again"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[self textFieldAtIndex:1] becomeFirstResponder];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
