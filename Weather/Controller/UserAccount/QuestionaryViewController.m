//
//  QuestionaryViewController.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "QuestionaryViewController.h"
#import "TKArrayDataSource.h"
#import "TKTextFieldTableViewCell.h"

static NSString * const QuestionaryViewControllerTextFieldsTableViewCellNibName = @"TKTextFieldTableViewCell";
static NSString * const QuestionaryViewControllerTextFieldsTableViewCellIdentifier = @"TextField";

NSString * const QuestionaryViewControllerTextFieldsDictionaryTitleKey = @"QuestionaryViewControllerTextFieldsDictionaryTitleKey";
NSString * const QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey = @"QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey";
NSString * const QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey = @"QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey";
NSString * const QuestionaryViewControllerTextFieldsDictionarySecureKey = @"QuestionaryViewControllerTextFieldsDictionarySecureKey";


@interface QuestionaryViewController () <UITextFieldDelegate>

@property (strong,nonatomic) TKArrayDataSource *arrayDataSource;

@end

@implementation QuestionaryViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.layer.borderColor = [self.view backgroundColor].CGColor;
    self.tableView.layer.borderWidth = 1.0;
    
    // DataSource
    __weak QuestionaryViewController *weakSelf = self;
    TKArrayDataSourceConfigureCellBlock configureCellBlock = ^(UITableViewCell *cell, NSDictionary *textFieldDictionary) {
        TKTextFieldTableViewCell * texFieldCell = (TKTextFieldTableViewCell *)cell;
        
        if (textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryTitleKey])
            texFieldCell.titleLabel.text = textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryTitleKey];
        if (textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey])
            texFieldCell.textField.placeholder = textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey];
        texFieldCell.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        if (textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey])
            texFieldCell.textField.autocapitalizationType = [textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey] integerValue];
        if (textFieldDictionary[QuestionaryViewControllerTextFieldsDictionarySecureKey])
            texFieldCell.textField.secureTextEntry = [textFieldDictionary[QuestionaryViewControllerTextFieldsDictionarySecureKey] boolValue];
        NSUInteger textFieldIndex = [[[self class] textFieldDictionaries] indexOfObjectPassingTest:^BOOL(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [obj[QuestionaryViewControllerTextFieldsDictionaryTitleKey] isEqualToString:textFieldDictionary[QuestionaryViewControllerTextFieldsDictionaryTitleKey]];
        }];
        texFieldCell.textField.returnKeyType = (textFieldIndex == [[[self class] textFieldDictionaries] count] - 1) ? UIReturnKeyGo : UIReturnKeyNext;
        texFieldCell.textField.tag = textFieldIndex;
        texFieldCell.textField.delegate = weakSelf;
        
    };
    self.arrayDataSource = [[TKArrayDataSource alloc] initWithTableView:self.tableView
                                                                  items:[[self class] textFieldDictionaries]
                                                         cellIdentifier:QuestionaryViewControllerTextFieldsTableViewCellIdentifier
                                                     configureCellBlock:configureCellBlock];
    
    // TableView
    self.tableView.dataSource = self.arrayDataSource;
    self.tableView.allowsSelection = NO;
    
    // Cell
    [self.tableView registerNib:[UINib nibWithNibName:QuestionaryViewControllerTextFieldsTableViewCellNibName
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:QuestionaryViewControllerTextFieldsTableViewCellIdentifier];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[self textFieldAtIndex:0] becomeFirstResponder];
}

#pragma mark - Public API

- (UITextField *)textFieldAtIndex:(NSUInteger)index {
    TKTextFieldTableViewCell *cell = [self.tableView
                                      cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index
                                                                               inSection:0]];
    return cell.textField;
}

- (NSString *)textAtIndex:(NSUInteger)index {
    return [self textFieldAtIndex:index].text;
}


#pragma mark - <UITextFieldDelegate>

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.returnKeyType == UIReturnKeyNext) {
        [[self textFieldAtIndex:(textField.tag + 1)] becomeFirstResponder];
        return YES;
    }
    else if (textField.returnKeyType == UIReturnKeyGo) {
        [textField resignFirstResponder];
        if ([self isAllFilled])
            [self didQuestionaryReady];
        else
            [self showNotAllFilledAlert];
        return YES;
    }
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Need to override

+ (NSArray *)textFieldDictionaries {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (void)didQuestionaryReady {
    return;
}


#pragma mark - Helper methods

- (BOOL)isAllFilled {
    NSUInteger textFieldIndex = 0;
    while (YES) {
        UITextField *textField = [self textFieldAtIndex:textFieldIndex];
        if (!textField) break;
        if (![textField.text length]) return NO;
        textFieldIndex++;
    }
    return YES;
}

- (void)showNotAllFilledAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Fill all fields"
                                                                             message:@"In order to proceed fill all fields"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:NULL]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

@end
