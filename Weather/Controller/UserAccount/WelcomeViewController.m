//
//  WelcomeViewController.m
//  Weather
//
//  Created by Timur on 28.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WelcomeViewController.h"
#import "TKArrayDataSource.h"
#import "TKButtonTableViewCell.h"
#import "LogInViewController.h"
#import "SignUpViewController.h"

static NSString * const kWelcomeViewControllerButtonsTableViewCellNibName = @"TKButtonTableViewCell";
static NSString * const kWelcomeViewControllerButtonsTableViewCellIdentifier = @"Button";

static NSString * const kWelcomeViewControllerButtonsDictionaryTitleKey = @"kWelcomeViewControllerButtonsDictionaryTitleKey";
static NSString * const kWelcomeViewControllerButtonsDictionarySegueKey = @"kWelcomeViewControllerButtonsDictionarySegueKey";

static NSString * const kWelcomeViewControllerLogInSegueIdentifier = @"LogIn";
static NSString * const kWelcomeViewControllerSignUpSegueIdentifier = @"SignUp";


@interface WelcomeViewController () <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *buttonsTableView;
@property (strong,nonatomic) TKArrayDataSource *arrayDataSource;

@end

@implementation WelcomeViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.buttonsTableView.layer.borderColor = [self.view backgroundColor].CGColor;
    self.buttonsTableView.layer.borderWidth = 1.0;
    
    // DataSource
    TKArrayDataSourceConfigureCellBlock configureCellBlock = ^(UITableViewCell *cell, NSString *buttonTitle) {
        ((TKButtonTableViewCell *)cell).titleLabel.text = buttonTitle;
    };
    self.arrayDataSource = [[TKArrayDataSource alloc] initWithTableView:self.buttonsTableView
                                                                  items:[[self buttonDictionaries] valueForKeyPath:kWelcomeViewControllerButtonsDictionaryTitleKey]
                                                         cellIdentifier:kWelcomeViewControllerButtonsTableViewCellIdentifier
                                                     configureCellBlock:configureCellBlock];
    self.buttonsTableView.dataSource = self.arrayDataSource;
    
    // Cell
    [self.buttonsTableView registerNib:[UINib nibWithNibName:kWelcomeViewControllerButtonsTableViewCellNibName
                                                      bundle:[NSBundle mainBundle]]
                forCellReuseIdentifier:kWelcomeViewControllerButtonsTableViewCellIdentifier];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [super viewWillDisappear:animated];
}

#pragma mark - User Interaction


#pragma mark - <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *buttonDictionary = [[self buttonDictionaries] objectAtIndex:indexPath.row];
    if (!buttonDictionary) return;
    NSString * segueIdentifier = buttonDictionary[kWelcomeViewControllerButtonsDictionarySegueKey];
    [self performSegueWithIdentifier:segueIdentifier sender:self];
}

#pragma mark - Helper methods

- (NSArray *)buttonDictionaries {
    return @[
             @{kWelcomeViewControllerButtonsDictionaryTitleKey: @"Log in with existing account",
               kWelcomeViewControllerButtonsDictionarySegueKey: kWelcomeViewControllerLogInSegueIdentifier
               },
             @{kWelcomeViewControllerButtonsDictionaryTitleKey: @"Create account",
               kWelcomeViewControllerButtonsDictionarySegueKey: kWelcomeViewControllerSignUpSegueIdentifier
               }
             ];
}


@end
