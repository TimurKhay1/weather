//
//  QuestionaryViewController.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TKArrayDataSource;

FOUNDATION_EXPORT NSString * const QuestionaryViewControllerTextFieldsDictionaryTitleKey;
FOUNDATION_EXPORT NSString * const QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey;
FOUNDATION_EXPORT NSString * const QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey;
FOUNDATION_EXPORT NSString * const QuestionaryViewControllerTextFieldsDictionarySecureKey;


/**
 *  The `QuestionaryViewController` is used for questionaries/worksheets.
 *  Typically can be used for log in screens, fill user info screens.
 *
 *  @warning This class is intended to be subclassed. You should not use it directly.
 */
@interface QuestionaryViewController : UIViewController

/**
 *  @warning Make an outlet to your table view in storyboard
 */
@property (weak, nonatomic) IBOutlet UITableView *tableView;

/**
 * Description of fields in Questionary.
 * Must be array of dicionaries with keys QuestionaryViewControllerTextFields%Key.
 * See header file for details.
 *
 *  @warning Make an outlet to your table view in storyboard
 */
+ (NSArray *)textFieldDictionaries;

/**
 * This method is gonna be called when all question are answered.
 * Do not call super.
*
 */
- (void)didQuestionaryReady;

/**
 * Use this method to access TextFields.
 *
 */

- (UITextField *)textFieldAtIndex:(NSUInteger)index;

/**
 * Use this method to access text filled in textfields.
 *
 */
- (NSString *)textAtIndex:(NSUInteger)index;

@end
