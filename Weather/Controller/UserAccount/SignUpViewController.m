//
//  SignUpViewController.m
//  Weather
//
//  Created by Timur on 28.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "SignUpViewController.h"
#import "WTAppDelegate+WTViewControllerRoutines.h"
#import "MagicalRecord/MagicalRecord.h"
#import "MBProgressHUD.h"
#import "WTUser.h"


@implementation SignUpViewController

#pragma mark - Overriding

+ (NSArray *)textFieldDictionaries {
    return @[
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"First name",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"First name",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeWords),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @NO
               },
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"Last name",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"Last name",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeWords),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @NO
               },
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"Username",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"Username",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeNone),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @NO
               },
             @{QuestionaryViewControllerTextFieldsDictionaryTitleKey: @"Password",
               QuestionaryViewControllerTextFieldsDictionaryPlaceholderKey: @"Password",
               QuestionaryViewControllerTextFieldsDictionaryCapitalizationKey: @(UITextAutocapitalizationTypeNone),
               QuestionaryViewControllerTextFieldsDictionarySecureKey: @YES
               }
             ];
}

- (void)didQuestionaryReady {
    NSString *firstName = [self textAtIndex:0];
    NSString *lastName = [self textAtIndex:1];
    NSString *username = [[self textAtIndex:2] lowercaseString];
    NSString *password = [self textAtIndex:3];
    
    
    // if has user with such name -> alert
    if ([WTUser MR_findFirstByAttribute:@"username" withValue:username]) {
        [self showAlreadyHasSuchUserAlert];
        return;
    }
    
    // create user
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperationWithBlock:^{
        WTUser *newUser = [WTUser MR_createEntityInContext:[NSManagedObjectContext MR_rootSavingContext]];
        newUser.firstName = firstName;
        newUser.lastName = lastName;
        newUser.username = username;
        newUser.password = password;
        [[NSManagedObjectContext MR_rootSavingContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (!contextDidSave) {
                [self showErrorSavingUserAlert];
                return;
            }
            [WTAppDelegate proceedToAppWithUserName:username];
        }];
    }];
}

#pragma mark - User Interaction

- (IBAction)didPressCreateButton:(UIBarButtonItem *)sender {
    [self didQuestionaryReady];
}


#pragma mark - Helper methods


- (void)showAlreadyHasSuchUserAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Allready has such user"
                                                                             message:@"Choose another Username"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[self textFieldAtIndex:2] becomeFirstResponder];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showErrorSavingUserAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error occured while saving user"
                                                                             message:@"Please try again"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Try again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self didQuestionaryReady];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:NULL]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end


