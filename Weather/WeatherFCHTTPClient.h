//
//  WeatherFCHTTPClient.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "WeatherHTTPClient.h"

@interface WeatherFCHTTPClient : AFHTTPSessionManager <WeatherHTTPClient>

@end
