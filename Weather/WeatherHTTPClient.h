//
//  WeatherHTTPClient.h
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#ifndef WeatherHTTPClient_h
#define WeatherHTTPClient_h

@protocol WeatherHTTPClientDelegate;

@protocol WeatherHTTPClient <NSObject>

@property (nonatomic, weak) id<WeatherHTTPClientDelegate>delegate;

+ (instancetype)sharedWeatherHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)updateWeatherAtLocation:(CLLocation *)location forNumberOfDays:(NSUInteger)number;

@end

@protocol WeatherHTTPClientDelegate <NSObject>
@optional
-(void)weatherHTTPClient:(id<WeatherHTTPClient>)client didUpdateWithWeather:(id)weather;
-(void)weatherHTTPClient:(id<WeatherHTTPClient>)client didFailWithError:(NSError *)error;
@end


#endif /* WeatherHTTPClient_h */
