//
//  WeatherFCHTTPClient.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WeatherFCHTTPClient.h"

static NSString * const ForecastURLString = @"https://api.forecast.io/forecast/41a65e883168ad64aaef8d358d270f0f/";


@implementation WeatherFCHTTPClient

@synthesize delegate = _delegate;

+ (instancetype)sharedWeatherHTTPClient
{
    static WeatherFCHTTPClient *_sharedWeatherHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:ForecastURLString]];
    });
    
    return _sharedWeatherHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)updateWeatherAtLocation:(CLLocation *)location forNumberOfDays:(NSUInteger)number {
    NSString *endPoint = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
    
    [self GET:endPoint parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(weatherHTTPClient:didUpdateWithWeather:)]) {
            responseObject = [[self class] normalizedResponseObject:responseObject];
            [self.delegate weatherHTTPClient:self didUpdateWithWeather:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(weatherHTTPClient:didFailWithError:)]) {
            [self.delegate weatherHTTPClient:self didFailWithError:error];
        }
    }];
}

// TODO: bad approach, but better not to have categories on dictionary but have weather objects and have mapping for each service
// gonna be much easier
+ (NSDictionary *)normalizedResponseObject:(NSDictionary *)responseObject {
    NSDictionary *convertedCurrent = [self convertedWeather:responseObject[@"currently"]];
    
    NSArray *nextDays = responseObject[@"daily"][@"data"];
    NSMutableArray *convertedNextDays = [[NSMutableArray alloc] init];
    for (NSDictionary *nextDay in nextDays) {
        NSDictionary *convertedNextDay = [self convertedWeather:nextDay];
        [convertedNextDays addObject:@{@"hourly": @[convertedNextDay]}];
    }
    
    
    
    return @{@"data": @{
                     @"current_condition" : @[convertedCurrent],
                     @"weather":convertedNextDays
                        }
             };
    
}

+ (NSDictionary *)convertedWeather:(NSDictionary *)weather {
    NSMutableDictionary *converted = [weather mutableCopy];
    
    NSString *icon = converted[@"icon"];
    NSString *weatherIconURL = [NSString stringWithFormat:@"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/%@.png", [[self class] iconURLDictionary][icon]];
    
    if (converted[@"temperature"]) converted[@"temp_C"] = @(([converted[@"temperature"] integerValue] - 32)/1.8f);
    if (converted[@"temperatureMin"]) converted[@"tempMinC"] = @(([converted[@"temperatureMin"] integerValue] - 32)/1.8f);
    if (converted[@"temperatureMax"]) converted[@"tempMaxC"] = @(([converted[@"temperatureMax"] integerValue] - 32)/1.8f);
    
    converted[@"weatherDesc"] = @[@{@"value": [icon stringByReplacingOccurrencesOfString:@"-" withString:@" "]}];
    converted[@"weatherIconUrl"] = @[@{@"value": weatherIconURL}];

    return converted;
}


+ (NSDictionary *)iconURLDictionary {
    return @{
      @"clear-day":     @"wsymbol_0001_sunny",
      @"clear-night":   @"wsymbol_0008_clear_sky_night",
      @"rain":          @"wsymbol_0018_cloudy_with_heavy_rain",
      @"snow":          @"wsymbol_0011_light_snow_showers",
      @"sleet":                 @"wsymbol_0013_sleet_showers",
      @"wind":                  @"wsymbol_0006_mist.png",
      @"fog":                   @"wsymbol_0007_fog",
      @"cloudy":                @"wsymbol_0003_white_cloud",
      @"partly-cloudy-day":     @"wsymbol_0002_sunny_intervals",
      @"partly-cloudy-night":   @"wsymbol_0008_clear_sky_night"
      };
}


@end
