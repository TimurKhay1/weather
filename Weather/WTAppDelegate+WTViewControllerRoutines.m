//
//  WTAppDelegate+WTViewControllerRoutines.m
//  Weather
//
//  Created by Timur on 29.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import "WTAppDelegate+WTViewControllerRoutines.h"
#import "NSUserDefaults+WTData.h"

static NSString * const kWTAppDelegateMainStoryboardIdentifier = @"MainStoryboard";

static NSString * const kWTAppDelegateNotAuthenticatedIdentifier = @"InitialWhenNotAuthenticated";
static NSString * const kWTAppDelegateAuthenticatedIdentifier = @"InitialWhenAuthenticated";


@implementation WTAppDelegate (WTViewControllerRoutines)

+ (void)instantiateRootViewControllerByAuthorizationStatus {
    NSString *username = [NSUserDefaults standardUserDefaults].WT_username;
    
    if (username)
        [self proceedToAppWithUserName:username];
    else
        [self proceedToAuthorization];
}

+ (void)proceedToAppWithUserName:(NSString *)username {
    [NSUserDefaults standardUserDefaults].WT_username = username;
    [self setRootViewControllerWithIdentifier:kWTAppDelegateAuthenticatedIdentifier];
}


+ (void)proceedToAuthorization {
    [NSUserDefaults standardUserDefaults].WT_username = nil;
    [self setRootViewControllerWithIdentifier:kWTAppDelegateNotAuthenticatedIdentifier];
}

+ (void)setRootViewControllerWithIdentifier:(NSString *)identifier {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:kWTAppDelegateMainStoryboardIdentifier bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:identifier];
    WTAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = vc;
}

@end
