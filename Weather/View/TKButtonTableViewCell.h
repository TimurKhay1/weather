//
//  TKButtonTableViewCell.h
//  Weather
//
//  Created by Timur on 28.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
