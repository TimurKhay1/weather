//
//  TKTextFieldTableViewCell.h
//  Weather
//
//  Created by Timur on 28.11.15.
//  Copyright © 2015 Scott Sherwood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKTextFieldTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end
